# Next.js Authentication

This is the [tutorial][u:tuto] from Auth0 to authenticate a user
on a [Next.js][u:next] app.

[u:tuto]: https://auth0.com/blog/next-js-authentication-tutorial/
[u:next]: https://nextjs.org

## Up & Running

```
$ cp .env.example .env # no sensitive data in here
$ nvm use
$ npm install
$ npm run dev
```

Your app should be running at http://localhost:3000/.

![App Homepage](.imgs/homepage.png)

Try to go to [/login](http://localhost:3000/login) and input

    username: john.doe
    password: password

You should be authenticated, and see the following screen:

![Logged in screenshot](.imgs/logged-in.png)
