require('dotenv').config();

const path = require('path');
const Dotenv = require('dotenv-webpack');

module.exports = {
  webpack: config => {
    return Object.assign({}, config, {
      plugins: (config.plugins || []).concat(
        new Dotenv({
          path: path.join(__dirname, '.env'),
          systemvars: true,
        }),
      ),
    });
  },
};
