const bodyParser = require('body-parser');
const { Router } = require('express');

const router = new Router();

router.use(bodyParser.json());

const thoughts = [
  { _id: 123, message: 'I love pepperoni pizza!', author: 'unknown' },
  { _id: 456, message: "I'm watching Netflix.", author: 'unknown' },
];

router.get('/api/thoughts', (req, res) => {
  const orderedThoughths = thoughts.sort((t1, t2) => t2._id - t1._id);
  res.send(orderedThoughths);
});

router.post('/api/thoughts', (req, res) => {
  const { message } = req.body;
  const newThought = {
    message,
    _id: new Date().getTime(),
    author: 'unkown',
  };
  thoughts.push(newThought);
  res.send({ message: 'Thanks!' });
});

module.exports = router;
