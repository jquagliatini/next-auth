const { Router } = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const jsonwebtoken = require('jsonwebtoken');

const router = new Router();

router.post(
  '/login',
  bodyParser.urlencoded(),
  passport.authenticate('local'),
  (req, res) => {
    res.cookie('jwt', jsonwebtoken.sign({ username: req.username }, 'secret'), {
      httpOnly: true,
    });
    res.redirect('/share-thoughts');
  },
);

module.exports = router;
