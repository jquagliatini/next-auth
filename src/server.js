require('dotenv').config();
const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const http = require('http');
const next = require('next');
const uid = require('uid-safe');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const thoughtsApi = require('./thoughts-api');
const authRoutes = require('./auth-routes');

const dev = process.env.NODE_ENV !== 'production';
const app = next({
  dev,
  dir: './src',
});
const handle = app.getRequestHandler();

const users = [
  {
    id: 1,
    username: 'john.doe',
    displayName: 'John Doe',
    picture: 'https://randomuser.me/api/portraits/men/1.jpg',
    password: 'password',
    created_at: new Date().toISOString(),
  },
  {
    id: 2,
    username: 'sara.jones',
    displayName: 'Sara Jones',
    picture: 'https://randomuser.me/api/portraits/women/91.jpg',
    password: 'password',
    created_at: new Date().toISOString(),
  },
];

app.prepare().then(() => {
  const server = express();

  const secret = uid.sync(18);

  server.use(cookieParser(secret));

  const sessionConfig = {
    secret,
    cookie: {
      maxAge: 86400 * 1000,
    },
    resave: false,
    saveUnitialized: true,
  };
  server.use(session(sessionConfig));

  passport.use(
    new LocalStrategy(function verify(username, password, done) {
      const user = users.find(u => u.username === username);

      if (!user) {
        return done(null, false, { message: 'Invalid username!' });
      }

      if (password !== user.password) {
        return done(null, false, { message: 'Invalid password!' });
      }

      return done(null, user);
    }),
  );

  passport.serializeUser((user, done) => {
    return done(null, user);
  });

  passport.deserializeUser((user, done) => {
    return done(null, user);
  });

  function extractJwtFromCookie(req) {
    return req.cookies ? req.cookies.jwt : null;
  }

  const jwtStrategy = new JwtStrategy(
    {
      secretOrKey: 'secret',
      jwtFromRequest: extractJwtFromCookie,
    },
    function verify({ email }, done) {
      return done({ email });
    },
  );
  passport.use(jwtStrategy);
  server.use(passport.initialize());
  server.use(passport.session());

  server.use(thoughtsApi);

  const restrictAccess = (req, res, next) => {
    if (!req.isAuthenticated()) return res.redirect('/login');
    next();
  };

  server.use(authRoutes);
  server.use('/profile', restrictAccess);
  server.use('/share-thought', restrictAccess);

  server.get('*', handle);

  http.createServer(server).listen(process.env.PORT, () => {
    console.log('listeninig on port %d', process.env.PORT);
  });
});
