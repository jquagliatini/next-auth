import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export default function Login() {
  return (
    <Container>
      <Form action="/login" method="POST">
        <Form.Group>
          <Form.Label>Username</Form.Label>
          <Form.Control name="username" type="text" />
          <Form.Text>your username</Form.Text>
        </Form.Group>
        <Form.Group>
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" name="password" />
          <Form.Text>Your password</Form.Text>
        </Form.Group>
        <Button type="submit" variant="primary">
          Login
        </Button>
      </Form>
    </Container>
  );
}
